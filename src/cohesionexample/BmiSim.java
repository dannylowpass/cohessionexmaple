/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cohesionexample;

import java.util.*;

/**
 *
 * @author dannylowpass
 */
public class BmiSim {
  
    public static void main(String[] args){
            
        Person p1 = new Person("Miguel", 82, Weight.KG, 1.72, Height.M);
        Person p2 = new Person("Javier", 152, Weight.LB, 70, Height.IN);
            
            
            
        List<Person> persons = new ArrayList<>();
            
        persons.add(p1);
        persons.add(p2);
            
        for(Person x: persons){
        System.out.printf("Name: %s BMI: %.2f\n", x.getName(), x.getBMI());
            
        }
    }
}