/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cohesionexample;

/**
 *
 * @author dannylowpass
 */
public class Person {
    
    private String name;
    private double height;
    private double weight;
    private Weight unitWeight;
    private Height unitHeight;
            
    
    public Person(){}
    
    public Person(String name, double weight, Weight unitWeight, double height,
            Height unitHeight){
        this.name = name;
        this.weight = weight;
        this.height = height;
        this.unitWeight = unitWeight;
        this.unitHeight = unitHeight;
    }
    
    public String getName(){
        return name;
    }
    
    public double getWeight(){
        return weight;
    }
    
    public double getHeight(){
       return height;
   }
   
    public void setName(String newName){
       this.name = newName;
   }
    
    public void setWeight(double newWeight){
       this.weight = newWeight;
   }
    
    public void setheight(double newheight){
       this.weight = newheight;
   }
    
    public double getBMI(){
        
        double weightInKilos = 0;
        double heightInMetres = 0;
        double bmi = 0;
        
        switch (unitWeight){
            case KG : weightInKilos = weight;
            break;
            case LB : weightInKilos = weight / 2.205;
        }
        
        switch (unitHeight){
            case M : heightInMetres = height;
            break;
            case IN : heightInMetres = height / 39.37;
            break;
            } 
        bmi = (weightInKilos/(Math.pow(heightInMetres, 2)));
        return bmi;
    } 
}